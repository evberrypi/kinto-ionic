import { Component } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment'
@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss']
})
export class AboutPage {
  logdatas: any;
  ghkey: any;
  constructor(public http: Http) {
      const timestamp = Date.now()
      
    }
    
    getlogs() {
      this.ghkey = environment.ghkey
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );
      headers.append('Authorization', 'Bearer ' + btoa('github' + ':' + this.ghkey));
      let options = new RequestOptions({ headers: headers });
     console.log(options)
      this.http.get("https://api.everettlacey.com/v1/buckets/tasks/collections/test/records", options)
        .subscribe(data => {
          console.log(data) //['_body']);
          this.logdatas = JSON.parse(data['_body']);
             }, error => {
          console.log(error);// Error getting the data
        });
    }
    delete(item){
      console.log(item)
      this.http.delete(`https://api.everettlacey.com/v1/buckets/tasks/collections/test/records/${item}`)
      .subscribe(data => {
        console.log("Successfully Deleted");
        // this.getlogs();
        console.log(data) //['_body']);
       }, error => {
        console.log("ERROR"); 
        console.log(error);// Error getting the data
      });
      this.getlogs();
    }
}
